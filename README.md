# generator-preact [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]
> Yeoman generator for preact.js (not react!!!) / mithril.js using mainly JSX with gulp , rollup and other goodies

## Installation

```sh
$ npm install --save generator-jsx
```

## Usage

```sh
$ yo jsx
```
## License

MIT © [Joel Chu](joelchu.com)


[npm-image]: https://badge.fury.io/js/generator-preact.svg
[npm-url]: https://npmjs.org/package/generator-preact
[travis-image]: https://travis-ci.org/joelchu/generator-preact.svg?branch=master
[travis-url]: https://travis-ci.org/joelchu/generator-preact
[daviddm-image]: https://david-dm.org/joelchu/generator-preact.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/joelchu/generator-preact
[coveralls-image]: https://coveralls.io/repos/joelchu/generator-preact/badge.svg
[coveralls-url]: https://coveralls.io/r/joelchu/generator-preact
