## Gulp function collection

You will find a list of exported function that passing back to the `gulpfile.js`
that makes the main file cleaner and easier to maintain. Do share any new functions
you have created with us. 
